<?php
/**
 * Modifies SilverStripe's standard GridFieldDeleteAction to allow  
 * unlinking or deletion based on the result of canDelete and canEdit  
 * forcing one or the other on all records.
 * @refer GridFieldDeleteAction
 * @package forms
 */
class GridFieldDeleteOrUnlinkAction extends GridFieldDeleteAction {

	/**
	 *
	 * @param GridField $gridField
	 * @param DataObject $record
	 * @param string $columnName
	 * @return string - the HTML for the column
	 */
	public function getColumnContent($gridField, $record, $columnName) {
        if($record->canDelete()) {
            return $this->deleteColumnContent($gridField,$record);
        } elseif($record->canEdit()) {
            return $this->unlinkColumnContent($gridField,$record);
        }
        
        return;
	}

    protected function unlinkColumnContent($gridField, $record) {
        
        $field = GridField_FormAction::create($gridField, 'UnlinkRelation'.$record->ID, false,
                "unlinkrelation", array('RecordID' => $record->ID))
            ->addExtraClass('gridfield-button-unlink')
            ->setAttribute('title', _t('GridAction.UnlinkRelation', "Unlink"))
            ->setAttribute('data-icon', 'chain--minus');
        
        return $field->Field();
    }
    
    protected function deleteColumnContent($gridField, $record) {
        
        $field = GridField_FormAction::create($gridField,  'DeleteRecord'.$record->ID, false, "deleterecord",
                array('RecordID' => $record->ID))
            ->addExtraClass('gridfield-button-delete')
            ->setAttribute('title', _t('GridAction.Delete', "Delete"))
            ->setAttribute('data-icon', 'cross-circle')
            ->setDescription(_t('GridAction.DELETE_DESCRIPTION','Delete'));
        
        return $field->Field();

    }

}
